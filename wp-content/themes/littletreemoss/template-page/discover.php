<?php
/**
 * Template Name: Découverte
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package littletreemoss
 */

get_header();
?>
<main id="discover" class="page discover">
        <!-- If is active -->
        <div class="discover-title-filters">
                <h1 class="discover-title">Découverte.</h1>
                <div id="filters" class="button-group filters">
                        <button class="button is-checked all" data-filter="*">Toutes</button>
                        <button class="button multimedia" data-filter=".multimedia">Multimédias</button>
                        <button class="button creativity" data-filter=".creativity">Créateurs</button>
                        <button class="button lifestyle" data-filter=".lifestyle">Lifestyle</button>
                        <button class="button beauty" data-filter=".beauty">Beauté</button>
                </div>
        </div>
        <p class="discover-intro">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus hendrerit lectus mauris, sed gravida
                eros imperdiet id. Nulla maximus pellentesque nibh, non accumsan purus. Aliquam erat volutpat. Proin
                egestas auctor magna eu sagittis. Sed ligula nibh, viverra sit amet posuere in, volutpat sit amet purus.
        </p>

        <div class="grid-isotop grid-discover">
                <?php $args = array(
                'posts_per_page' => 10000, /* how many post you need to display */
                'offset' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'articles', /* your post type name */
                'post_status' => 'publish'
                );
                $query = new WP_Query($args);
                if ($query->have_posts()) :
                        while ($query->have_posts()) : $query->the_post();
                        $categories = get_the_category();            ?>
                <a class="element-item <?php echo $categories[0]->slug;?>" href="<?php the_permalink(); ?>"
                        data-category="<?php echo $categories[0]->slug;?>">
                        <div class="discover-card discover-card--<?php echo $categories[0]->slug;?>">
                                <?php 
            $image = get_field('article_image');
            if( !empty( $image ) ): ?>
                                <div class="discover-card_image">
                                        <img src="<?php echo esc_url($image['url']); ?>"
                                                alt="<?php echo esc_attr($image['alt']); ?>" />
                                </div>
                                <?php endif; ?>
                                <div class="discover-card_body">
                                        <h3 class="discover-card_body-title"><?php the_field('article_title') ?></h3>
                                        <i class="bi bi-arrow-right icon"></i>
                                </div>
                        </div>
                </a>
                <?php
                        endwhile;
                endif;
                ?>
        </div>
</main><!-- #main -->
<?php
get_footer();