<?php
/**
 * Template Name: Galerie
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package littletreemoss
 */

get_header();
?>
<main id="gallery" class="page gallery">
        <!-- If is active -->
        <div class="gallery-title-filters">
                <h1 class="gallery-title">Galerie.</h1>
                <div id="filters" class="button-group filters">
                        <button class="button is-checked all" data-filter="*">Toutes</button>
                        <button class="button digital" data-filter=".digital">Digital</button>
                        <button class="button acrylic" data-filter=".acrylic">Acrylique</button>
                </div>
        </div>
        <div class="grid-isotop grid-gallery">
                <?php $args = array(
                'posts_per_page' => 10000, /* how many post you need to display */
                'offset' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'dessins', /* your post type name */
                'post_status' => 'publish'
                );
                $query = new WP_Query($args);
                if ($query->have_posts()) :
                        while ($query->have_posts()) : $query->the_post();
                        $categories = get_the_category();            ?>
                <a class="element-item <?php echo $categories[0]->slug;?>" href="<?php the_permalink(); ?>"
                        data-category="<?php echo $categories[0]->slug;?>">
                        <div class="gallery-card gallery-card--<?php echo $categories[0]->slug;?>">
                                <?php 
            $image = get_field('draw_image');
            if( !empty( $image ) ): ?>
                                <div class="gallery-card_image">
                                        <img src="<?php echo esc_url($image['url']); ?>"
                                                alt="<?php echo esc_attr($image['alt']); ?>" />
                                </div>
                                <?php endif; ?>
                                <div class="gallery-card_body">
                                        <h3 class="gallery-card_body-title"><?php the_field('draw_title') ?></h3>
                                        <i class="bi bi-arrow-right icon"></i>
                                </div>
                        </div>
                </a>
                <?php
                        endwhile;
                endif;
                ?>
        </div>
</main><!-- #main -->
<?php
get_footer();