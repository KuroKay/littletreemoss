<?php
/**
 * Template part for displaying a intro message
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package littletreemoss
 */

?>

<section class="block_intro">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="block_intro_element">
                    <?php 
                        $image = get_field('block_intro_element');
                        if( !empty( $image ) ): ?>
                            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    <?php endif; ?>
                </div>
                <h1 class="block_intro_title"><?php the_field('block_intro_title'); ?></h1>
                <p><?php the_field('block_intro_desc'); ?></p>
            </div>
            <div class="col-6">
                <div class="block_intro_image">
                    <?php 
                        $image = get_field('block_intro_image');
                        if( !empty( $image ) ): ?>
                            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<style type="text/css">
	.block_intro {
		background: <?php the_field('block_intro_background'); ?>;
		color: <?php the_field('block_intro_color'); ?>;
	}
</style>